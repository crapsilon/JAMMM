use std::path::PathBuf;

use chrono::{
    serde::{ts_milliseconds, ts_milliseconds_option},
    DateTime, Utc,
};
use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Serialize)]
pub struct Instance {
    #[serde(with = "ts_milliseconds_option")]
    pub last_time_started: Option<DateTime<Utc>>,
    pub name: String,
    pub curse_id: Option<u32>,
    pub icon: Option<InstanceIcon>,
    #[serde(with = "ts_milliseconds")]
    pub last_updated: DateTime<Utc>,
}

#[derive(Debug, Deserialize, Serialize)]
pub enum InstanceIcon {
    Path(PathBuf),
    Base64(String),
}

impl Instance {
    pub fn new(name: &str, curse_id: Option<u32>) -> Instance {
        Instance {
            last_time_started: None,
            name: name.to_owned(),
            curse_id,
            icon: None,
            last_updated: Utc::now(),
        }
    }

    /// Checks if `Instance` is custom or downloaded from Curse, i.e. if `Instance` has a `curse_id`.
    ///
    /// # Examples
    ///
    /// ```
    /// # use modpack_management::Instance;
    /// let i = Instance::new("Test Modpack", None);
    /// assert_eq!(i.is_custom(), true);
    /// ```
    pub fn is_custom(&self) -> bool {
        self.curse_id.is_none()
    }
}

#[cfg(test)]
mod test {

    use chrono::{TimeZone, Utc};

    use super::Instance;

    fn custom_instance(name: &str) -> Instance {
        Instance::new(name, None)
    }

    fn curse_instance(name: &str) -> Instance {
        Instance::new(name, Some(42))
    }

    #[test]
    fn test_custom_instance() {
        let instance = custom_instance("Test Modpack");

        assert_eq!(instance.is_custom(), true)
    }

    #[test]
    fn test_curse_instance() {
        let instance = curse_instance("Test Modpack");

        assert_eq!(instance.is_custom(), false)
    }

    #[test]
    fn test_instance_deserialization() {
        let instance: Instance =
            serde_cbor::from_slice(include_bytes!("../test/assets/basic_test_modpack.cbor"))
                .unwrap();

        assert_eq!(instance.name, "Test Modpack".to_string());
    }

    #[test]
    fn test_instance_serialization() {
        let mut instance = Instance::new("Test Modpack", None);
        instance.last_updated = Utc.timestamp(0, 0).into();
        let bytes = serde_cbor::to_vec(&instance).unwrap();

        assert_eq!(
            &bytes,
            include_bytes!("../test/assets/basic_test_modpack.cbor")
        );
    }
}
