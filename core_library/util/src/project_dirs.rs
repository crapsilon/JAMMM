use std::path::Path;

use directories_next::ProjectDirs;
use lazy_static::lazy_static;

lazy_static! {
    static ref PROJECT_DIRS: ProjectDirs = ProjectDirs::from("org", "crapsilon", "JAMMM")
        .expect("error while evaluating project dirs");
}

pub fn get_cache_dir() -> &'static Path {
    lazy_static! {
        static ref CACHE: &'static Path = PROJECT_DIRS.cache_dir();
    }

    &CACHE
}

pub fn get_config_dir() -> &'static Path {
    lazy_static! {
        static ref CONFIG: &'static Path = PROJECT_DIRS.config_dir();
    }

    &CONFIG
}

pub fn get_data_dir() -> &'static Path {
    lazy_static! {
        static ref DATA: &'static Path = PROJECT_DIRS.data_dir();
    }

    &DATA
}
