use std::io;

use tokio::fs::create_dir_all;

#[inline(always)]
pub async fn create_data_folder() -> io::Result<()> {
    create_dir_all(crate::project_dirs::get_data_dir()).await
}

#[inline(always)]
pub async fn create_cache_folder() -> io::Result<()> {
    create_dir_all(crate::project_dirs::get_cache_dir()).await
}

#[inline(always)]
pub async fn create_config_folder() -> io::Result<()> {
    create_dir_all(crate::project_dirs::get_config_dir()).await
}

#[inline(always)]
pub async fn create_all_folders() -> io::Result<()> {
    create_data_folder().await?;
    create_cache_folder().await?;
    create_config_folder().await?;

    Ok(())
}
