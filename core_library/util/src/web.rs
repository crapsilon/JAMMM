use std::path::{Path, PathBuf};

use anyhow::Result;
use lazy_static::lazy_static;
use reqwest::{Body, IntoUrl};
use serde::{de::DeserializeOwned, Serialize};
use tokio::{fs, io::AsyncWriteExt};
use tokio_stream::StreamExt;

lazy_static! {
    static ref CLIENT: reqwest::Client = reqwest::ClientBuilder::new()
        .user_agent(concat!("jammm-v", env!("CARGO_PKG_VERSION")))
        .build()
        .expect("without a client the whole program can not run");
}

pub struct Client {
    client: reqwest::Client,
    base_path: PathBuf,
}

impl Client {
    /// Creates a new client that uses the global connection pool with a `base_path` on wich the filenames get appended
    /// (e.g. [`download_file`](self::Client::download_file)).
    ///
    /// # Examples
    ///
    /// ```no_run
    /// # use util::web::Client;
    /// let client = Client::new("/tmp");
    /// ```
    pub fn new<P: AsRef<Path>>(base_path: P) -> Client {
        Client {
            client: CLIENT.clone(),
            base_path: base_path.as_ref().to_path_buf(),
        }
    }

    /// Makes an API `GET` call and returns the deserialized json data.
    pub async fn api_get<T: DeserializeOwned, U: IntoUrl>(&self, url: U) -> reqwest::Result<T> {
        self.client.get(url).send().await?.json().await
    }

    /// Same as [`api_get`](self::Client::api_get) but also accepts a JSON body for the call.
    pub async fn api_get_body<T, U, B>(&self, url: U, body: &B) -> reqwest::Result<T>
    where
        T: DeserializeOwned,
        U: IntoUrl,
        B: Serialize + ?Sized,
    {
        self.client.get(url).json(body).send().await?.json().await
    }

    /// Makes a `GET` request and returns the response data as text.
    pub async fn http_get<U: IntoUrl>(&self, url: U) -> reqwest::Result<String> {
        self.client.get(url).send().await?.text().await
    }

    /// Same as [`http_get`](self::Client::http_get) but also accepts a body for the call.
    pub async fn http_get_body<U: IntoUrl, B: Into<Body>>(
        &self,
        url: U,
        body: B,
    ) -> reqwest::Result<String> {
        self.client.get(url).body(body).send().await?.text().await
    }

    /// Downloads a single file from the given url and saves it to the given file.
    ///
    /// It uses an internal buffer and streams the data in chunks to the disk.
    ///
    /// # Examples
    ///
    /// ```no_run
    /// # async fn run() {
    /// # use util::web::Client;
    ///
    /// let client = Client::new("/tmp");
    /// client
    ///     .download_file(
    ///         "https://codeberg.org/crapsilon/JAMMM/src/branch/develop/README.md",
    ///         "README.md",
    ///     )
    ///     .await
    ///     .unwrap();
    /// # }
    /// ```
    pub async fn download_file<U: IntoUrl, F: AsRef<Path>>(
        &self,
        url: U,
        filename: F,
    ) -> Result<()> {
        // `.chunk()` uses internally this stream and with this implementation we can avoid the extra function calls
        let mut stream = self.client.get(url).send().await?.bytes_stream();

        // because `join` is a method of `Path` this does not modify the `base_path`,
        // but returns a copy with the filename
        let path = self.base_path.join(filename);

        // create a new file to write to and fail if it does exist
        let mut file = fs::OpenOptions::new()
            .create_new(true)
            .write(true)
            .open(path)
            .await?;

        // custom buffer implementation because `BufWriter` does not work
        let mut buf = Vec::with_capacity(64 * 1024);

        // Get all chunks of data and write it to the buffer.
        // If the buffer gets full write the buffer to the file.
        while let Some(chunk) = stream.try_next().await? {
            if buf.len() + chunk.len() > buf.capacity() {
                file.write_all(&buf).await?;
                buf.clear();
            }
            buf.extend(chunk);
        }

        // write the remaining data in the buffer to the file before closing
        file.write_all(&buf).await?;

        Ok(())
    }
}

#[cfg(test)]
mod test {
    use std::env;

    use serde::Deserialize;
    use tokio::fs;

    use crate::Client;

    const URL: &str = "https://codeberg.org/crapsilon/JAMMM/src/branch/develop/core_library/util/test/assets/client_test.json";

    #[derive(Debug, Deserialize, PartialEq)]
    struct TestStruct {
        one: u8,
        two: u8,
    }

    #[ignore] // TODO (#95)
    #[tokio::test]
    async fn test_json() {
        let client = Client::new(env::temp_dir());

        let res: TestStruct = client.api_get(URL).await.unwrap();

        assert_eq!(res, TestStruct { one: 1, two: 2 });
    }

    #[ignore] // TODO (#95)
    #[tokio::test]
    async fn test_text() {
        let client = Client::new(env::temp_dir());

        let res = client.http_get(URL).await.unwrap();

        assert_eq!(res, include_str!("../test/assets/client_test.json"));
    }

    #[ignore] // TODO (#95)
    #[tokio::test]
    async fn test_file_download() {
        let client = Client::new(env::temp_dir());

        client.download_file(URL, "test.json").await.unwrap();

        let path = &env::temp_dir().join("test.json");

        let orig = include_bytes!("../test/assets/client_test.json");
        let downloaded = fs::read(path).await.unwrap();

        assert_eq!(orig, downloaded.as_slice());
    }
}
