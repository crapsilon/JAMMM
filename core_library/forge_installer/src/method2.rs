use std::{
    collections::HashMap,
    fs::{copy, create_dir_all, read_dir, remove_dir_all, remove_file, File, ReadDir},
    io::{Read, Write},
    path::Path,
};

use anyhow::Result;
use serde_json::Value;
use zip_extensions::zip_create_from_directory;

use crate::install_utils::{
    self, configure_version, download_lib, AdditionalLibrary, Lib, MainJson, VersionJson,
    INSTANCES_FOLDER, MINECRAFT_LIBRARIES_FOLDER, MINECRAFT_VERSION_FOLDER,
};

pub async fn install_forge_method2(forge_json: MainJson, path: &Path) -> Result<()> {
    if let Some(version_json_str) = &forge_json.version_json {
        let version_json: VersionJson = serde_json::from_str(version_json_str).unwrap();
        unreachable!("There was a version json: {:#?}", version_json); // check if even possible to reach this here
    }

    configure_version(&forge_json, path).await;

    let download_url = forge_json.download_url.as_ref().unwrap();

    if download_url.ends_with(".jar") {
        let forge = install_utils::Lib {
            name: "forge",
            path: &Path::new(MINECRAFT_LIBRARIES_FOLDER).join(
                &install_utils::process_maven_string(&forge_json.maven_version_string.unwrap()),
            ),
            url: download_url,
        };
        download_lib(&forge, path).await?; // also not sure if this can be reached
        println!("Downloaded a .jar")
    } else if download_url.ends_with(".zip") {
        if let Some(additional_files) = forge_json.additional_files_json.as_ref() {
            let libraries: Vec<AdditionalLibrary> = serde_json::from_str(additional_files).unwrap();
            for lib in libraries {
                let lib_url = lib.download_url;
                let file_name = lib_url.split('/').last().unwrap();

                let lib_path = INSTANCES_FOLDER.to_owned()
                    + &lib
                        .install_location
                        .replace(r"{filename}", file_name)
                        .replace(r"{instancefolder}", &forge_json.name)
                        .replace('\\', r"/");
                let download = Lib {
                    name: "",
                    url: &lib_url,
                    path: Path::new(&lib_path),
                };
                download_lib(&download, path).await?;
            }
        }
        let forge = Lib {
            name: "forge",
            url: download_url,
            path: &Path::new(MINECRAFT_VERSION_FOLDER)
                .join(forge_json.name.clone() + "/" + forge_json.filename.as_ref().unwrap()),
        };
        download_lib(&forge, path).await?;
        patch_jar(&forge_json, path).await?;
    }

    Ok(())
}

pub async fn patch_jar(forge_json: &MainJson, path: &Path) -> Result<()> {
    let version_path = path.join(MINECRAFT_VERSION_FOLDER.to_owned() + &forge_json.name);
    let mut version_json = File::open(version_path.join(forge_json.name.to_owned() + ".json"))?;
    // Allocating a moderate amount of memory, for less copy operations
    let mut buffer = Vec::with_capacity(16 * 1024);
    version_json.read_to_end(&mut buffer)?;
    let mut json: HashMap<&str, serde_json::Value> = serde_json::from_slice(&buffer)?;
    json.insert("id", Value::String(forge_json.name.to_owned()));
    json.remove_entry("downloads");

    let new_content = serde_json::to_string(&json).expect("unable to serialize");

    let mut version_json_new = std::fs::OpenOptions::new()
        .write(true)
        .truncate(true)
        .open(version_path.join(&forge_json.name).with_extension("json"))?;

    version_json_new.write_all(new_content.as_bytes())?;

    let jar_path = version_path.join(&forge_json.name).with_extension("jar");
    let jar_file = File::open(&jar_path)?;
    let mut jar_zipped = zip::ZipArchive::new(jar_file)?;
    let extracted_path = jar_path.parent().unwrap().join("unzipped_jar/");
    jar_zipped.extract(&extracted_path)?;

    let zip_path = version_path.join(
        forge_json
            .filename
            .as_ref()
            .expect("In this install method this json field should always exist"),
    );
    let zip_f = File::open(&zip_path)?;
    let mut zip_archive = zip::ZipArchive::new(zip_f)?;
    let extracted_path_zip = zip_path.parent().unwrap().join("unzipped_zip/");
    zip_archive.extract(&extracted_path_zip)?;

    let old_zip_folder = read_dir(extracted_path_zip)?;

    move_dir_contents(old_zip_folder, jar_path.parent().unwrap())?;

    remove_dir_all(jar_path.parent().unwrap().join("unzipped_jar/META-INF"))?;
    remove_file(&jar_path)?;
    zip_create_from_directory(&jar_path, &jar_path.parent().unwrap().join("test"))?;

    remove_dir_all(jar_path.parent().unwrap().join("unzipped_jar/"))?;
    remove_dir_all(jar_path.parent().unwrap().join("unzipped_zip/"))?;

    Ok(())
}

fn move_dir_contents(dir: ReadDir, base_path: &Path) -> Result<()> {
    for file in dir {
        let f = file.unwrap();
        if f.path().is_dir() {
            move_dir_contents(read_dir(f.path()).unwrap(), base_path)?;
        } else {
            let full_path = f.path();
            let rel_path_folder = full_path
                .parent()
                .unwrap()
                .strip_prefix(base_path.join("unzipped_zip"))?;
            let rel_path_file = full_path.strip_prefix(base_path.join("unzipped_zip"))?;

            create_dir_all(base_path.join("unzipped_jar").join(rel_path_folder)).unwrap();
            copy(f.path(), base_path.join("unzipped_jar").join(rel_path_file)).unwrap();
        }
    }

    Ok(())
}
