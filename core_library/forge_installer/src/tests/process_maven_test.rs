use crate::install_utils::process_maven_string;

#[test]
fn test_maven_strings() {
    let ins = vec![
        "[net.minecraft:client:1.17.1-20210706.113038:srg]",
        "[net.minecraft:client:1.17.1-20210706.113038:srg@dings]",
        "[net.minecraftforge:forge:1.17.1-37.0.126:client]",
        "[de.oceanlabs.mcp:mcp_config:1.17.1-20210706.113038:mappings-merged@txt]",
        "[net.minecraft:client:1.17.1-20210706.113038:slim]",
        "[net.minecraft:client:1.17.1-20210706.113038:extra]",
    ];
    let ergs = vec![
            "net/minecraft/client/1.17.1-20210706.113038/client-1.17.1-20210706.113038-srg.jar",
            "net/minecraft/client/1.17.1-20210706.113038/client-1.17.1-20210706.113038-srg.dings",
            "net/minecraftforge/forge/1.17.1-37.0.126/forge-1.17.1-37.0.126-client.jar",
            "de/oceanlabs/mcp/mcp_config/1.17.1-20210706.113038/mcp_config-1.17.1-20210706.113038-mappings-merged.txt",
            "net/minecraft/client/1.17.1-20210706.113038/client-1.17.1-20210706.113038-slim.jar",
            "net/minecraft/client/1.17.1-20210706.113038/client-1.17.1-20210706.113038-extra.jar",
        ];

    for i in 0..ins.len() {
        let erg = process_maven_string(ins[i]);
        assert_eq!(erg, ergs[i]);
    }
}
