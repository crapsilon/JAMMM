use std::{collections::HashMap, path::Path};

use anyhow::Result;
use serde::Deserialize;
use tokio::{
    fs::{copy, create_dir, create_dir_all, File},
    io::AsyncWriteExt,
};

pub(super) const MINECRAFT_VERSION_FOLDER: &str = "minecraft/versions/";
pub(super) const MINECRAFT_LIBRARIES_FOLDER: &str = "minecraft/libraries/";
pub(super) const INSTANCES_FOLDER: &str = "instances/";
const MINECRAFT_VERSION_BASE_URL: &str =
    "https://addons-ecs.forgesvc.net/api/v2/minecraft/version/";

#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct MainJson {
    pub install_method: u8,
    pub version_json: Option<String>,
    pub install_profile_json: Option<String>,
    pub minecraft_version: String,
    pub name: String,
    pub download_url: Option<String>,
    pub maven_version_string: Option<String>,
    pub additional_files_json: Option<String>,
    pub filename: Option<String>,
}
#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct AdditionalLibrary {
    pub download_url: String,
    pub install_location: String,
}

#[derive(Deserialize, Debug)]
pub struct InstallProfileJson {
    pub libraries: Vec<LibraryJson>,
    pub processors: Vec<ProcessorJson>,
    pub data: HashMap<String, DataEntry>,
}

#[derive(Deserialize, Debug)]
pub struct DataEntry {
    pub client: String,
    pub server: String,
}

#[derive(Deserialize, Debug)]
pub struct ProcessorJson {
    pub jar: String,
    pub classpath: Vec<String>,
    pub args: Vec<String>,
}

#[derive(Deserialize, Debug)]
pub struct LibraryJson {
    pub name: String,
    pub downloads: DownloadsJSON,
}
#[derive(Deserialize, Debug)]
pub struct DownloadsJSON {
    pub artifact: ArtifactJson,
}

#[derive(Deserialize, Debug)]
pub struct ArtifactJson {
    pub path: String,
    pub url: String,
}

#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct ResponseJson {
    pub json_download_url: String,
    pub jar_download_url: String,
}

#[derive(Deserialize, Debug)]
pub struct VersionJson {
    pub libraries: Vec<Library>,
}

#[derive(Deserialize, Debug)]
pub struct Library {
    pub name: String,
    pub url: Option<String>,
}

pub struct Lib<'a> {
    pub name: &'a str,
    pub path: &'a Path,
    pub url: &'a str,
}

pub async fn configure_version(forge_json: &MainJson, path: &Path) {
    let minecraft_version = &forge_json.minecraft_version;
    let forge_version = &forge_json.name;
    let vanilla_path = path.join(MINECRAFT_VERSION_FOLDER.to_owned() + minecraft_version);
    let version_path = path.join(MINECRAFT_VERSION_FOLDER.to_owned() + forge_version);
    create_dir_all(&vanilla_path).await.unwrap();
    create_dir_all(&version_path).await.unwrap();
    let version_json_path = version_path.join(forge_version).with_extension("json");
    let text_res = reqwest::get(MINECRAFT_VERSION_BASE_URL.to_owned() + minecraft_version)
        .await
        .unwrap()
        .text()
        .await
        .unwrap(); // TODO (#89)
    let json_res: ResponseJson = serde_json::from_str(&text_res).unwrap();
    for file_type in [".jar", ".json"] {
        let url = match file_type {
            ".jar" => &json_res.jar_download_url,
            ".json" => &json_res.json_download_url,
            _ => todo!(), // implement an Error
        };
        let data = reqwest::get(url).await.unwrap().bytes().await.unwrap();
        let file_path = vanilla_path
            .join(minecraft_version)
            .with_extension(file_type);
        let mut file = File::create(file_path).await.unwrap();
        file.write_all(&data).await.unwrap();
    }
    if let Some(version_json_str) = forge_json.version_json.as_ref() {
        let mut file = File::create(version_json_path).await.unwrap();
        file.write_all(version_json_str.as_bytes()).await.unwrap();
    } else {
        // This is needed for older Forge versions
        copy(
            vanilla_path.join(minecraft_version).with_extension(".json"),
            version_path.join(forge_version).with_extension(".json"),
        ) // needed because you must patch the original jar to get Forge
        .await
        .unwrap();
    }
    copy(
        vanilla_path.join(minecraft_version).with_extension(".jar"),
        version_path.join(forge_version).with_extension(".jar"),
    )
    .await
    .unwrap(); // TODO (#89)
}

pub async fn download_lib<'a>(lib: &Lib<'_>, path: &Path) -> Result<()> {
    let lib_url = lib.url;
    if lib_url.is_empty() {
        return Ok(());
    }
    let dir = path.join(lib.path);
    let prefix = dir.parent().unwrap();
    create_dir_all(prefix).await?;
    let mut file = File::create(&dir).await?;
    let response = reqwest::get(lib_url.to_owned()).await?;
    file.write_all(&response.bytes().await?).await?;
    if lib.name.contains("@lzma") {
        // This is needed for patching because the download name is not the same as the one needed for patching
        create_dir("/tmp/Jammm_data").await?; // TODO(#80)
        copy(&dir, "/tmp/Jammm_data/client.lzma").await?; // TODO(#80)
    }

    Ok(())
}

pub fn process_maven_string(maven_string: &str) -> String {
    // get the parts of the maven String and ignore surrounding brackets
    let parts = if maven_string.starts_with('[') {
        maven_string[1..maven_string.len() - 1]
            .split(':')
            .collect::<Vec<&str>>()
    } else {
        maven_string.split(':').collect::<Vec<&str>>()
    };

    // Change the '.'s to "/"s to get a valid path
    let mut finished = parts[0].replace('.', "/") + "/" + parts[1] + "/";

    // There are two different types of maven strings one with four elements and one with three
    if parts.len() == 4 {
        // add the last subfolder and start the file-name
        finished = finished + parts[2] + "/" + parts[1] + "-" + parts[2] + "-";
        // check if a specific file-extension is given with an '@' at the end
        finished += &sub_process_maven(parts[3]);
    // Does essentially the same as the above
    } else if parts.len() == 3 {
        finished = finished + parts[2].split('@').next().unwrap() + "/" + parts[1] + "-";
        finished += &sub_process_maven(parts[2]);
    }

    // return the finished string as a valid path
    finished
}

#[inline(always)]
fn sub_process_maven(part: &str) -> String {
    if part.contains('@') {
        part.replace('@', ".")
    } else {
        part.to_owned() + ".jar"
    }
}
