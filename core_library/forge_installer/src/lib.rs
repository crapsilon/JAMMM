mod install_utils;
mod method1;
mod method2;
mod method3;

#[cfg(test)]
mod tests;

use std::path::Path;

use anyhow::Result;

use install_utils::MainJson;
use method1::install_forge_method1;
use method2::install_forge_method2;
use method3::install_forge_method3;

const BASE_FORGE_URL: &str = "https://addons-ecs.forgesvc.net/api/v2/minecraft/modloader/";

pub async fn install_forge(version: &str, path: &Path) -> Result<()> {
    let full_url = BASE_FORGE_URL.to_owned() + version;
    let forge_json_string = reqwest::get(full_url).await?.bytes().await?;
    let forge_json: MainJson = serde_json::from_slice(&forge_json_string)?;
    match forge_json.install_method {
        1 => {
            install_forge_method1(forge_json, path).await?;
        }
        2 => {
            install_forge_method2(forge_json, path).await?;
        }
        3 => {
            install_forge_method3(forge_json, path).await;
        }
        _ => todo!(), // TODO(89): Implement a Custom Error if received install method is not in 1..3
    }

    Ok(())
}
