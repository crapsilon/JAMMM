use std::{
    collections::HashMap,
    fs::{remove_dir_all, File},
    io::Read,
    path::Path,
    process::Command,
};

use lazy_static::lazy_static;
use regex::Regex;

use crate::install_utils::{
    self, DataEntry, InstallProfileJson, LibraryJson, MainJson, ProcessorJson,
    MINECRAFT_LIBRARIES_FOLDER, MINECRAFT_VERSION_FOLDER,
};

pub async fn install_forge_method3(forge_json: MainJson, path: &Path) {
    let _ = remove_dir_all("/tmp/Jammm_data"); // TODO(#80)
    let install_profile_json: InstallProfileJson =
        serde_json::from_str(forge_json.install_profile_json.as_ref().unwrap()).unwrap();
    download_libraries(&install_profile_json.libraries, path).await;
    install_utils::configure_version(&forge_json, path).await;

    for processor in install_profile_json.processors.iter() {
        process_processor(
            processor,
            &install_profile_json.data,
            path,
            &forge_json.minecraft_version,
        )
        .await;
    }
    remove_dir_all("/tmp/Jammm_data").unwrap(); // TODO(#80)
}

fn build_path(s: &str) -> String {
    let parts = s.split(':').collect::<Vec<&str>>();
    MINECRAFT_LIBRARIES_FOLDER.to_owned()
        + &parts[0].replace('.', "/")
        + "/"
        + parts[1]
        + "/"
        + parts[2]
        + "/"
        + parts[1]
        + "-"
        + parts[2]
        + ".jar"
}

async fn process_processor(
    processor: &ProcessorJson,
    data: &HashMap<String, DataEntry>,
    path: &Path,
    minecraft_version: &str,
) {
    let full_class_path = path.join(build_path(&processor.jar));
    let mut classpaths = vec![full_class_path];
    for classpath in &processor.classpath {
        let full_path = path.join(build_path(classpath));
        classpaths.push(full_path);
    }
    let main_file_path = classpaths[0].to_owned();
    let main_file = File::open(main_file_path).unwrap();
    let mut zip = zip::ZipArchive::new(main_file).unwrap();
    let mut manifest_file = zip.by_name("META-INF/MANIFEST.MF").unwrap();
    let mut manifest_file_content = String::new();
    manifest_file
        .read_to_string(&mut manifest_file_content)
        .unwrap();
    let lines = manifest_file_content.split('\n');
    let mut main_class = "";
    for line in lines {
        if line.starts_with("Main-Class") {
            main_class = line.strip_prefix("Main-Class: ").unwrap();
            break;
        }
    }
    let joined = classpaths
        .iter()
        .map(|x| -> &str { x.as_os_str().to_str().unwrap() })
        .collect::<Vec<&str>>()
        .join(":");
    let mut args = ["-cp".to_owned(), joined, main_class.to_owned()].to_vec();
    args.append(&mut resolve_args(&data, processor, path, minecraft_version).await);

    let workdir_parts = classpaths[0]
        .as_os_str()
        .to_str()
        .unwrap()
        .split('/')
        .collect::<Vec<&str>>();
    let workdir = workdir_parts[0..workdir_parts.len() - 1].join("/");
    let _k = Command::new("java")
        .args(args)
        .current_dir(workdir)
        .output()
        .unwrap();
    println!("{:?}", std::str::from_utf8(_k.stderr.as_slice()).unwrap()); // TODO (89) Throw custom error on fallig java patching
}

async fn resolve_args(
    data: &&HashMap<String, DataEntry>,
    processor: &ProcessorJson,
    path: &Path,
    minecraft_version: &str,
) -> Vec<String> {
    lazy_static! {
        static ref RE: Regex = Regex::new(r"\[.*]$").unwrap();
    };
    let mut args = Vec::new();
    for arg in &processor.args {
        let mut res;
        // Different between the different types of arguments in the java command
        if arg.contains("MINECRAFT_JAR") {
            res = path
                .join(
                    MINECRAFT_VERSION_FOLDER.to_owned()
                        + minecraft_version
                        + "/"
                        + minecraft_version
                        + ".jar",
                )
                .as_os_str()
                .to_str()
                .unwrap()
                .to_owned();
        } else if arg.contains("BINPATCH") {
            // For some reason "BINPATCH" means the client.lzma
            res = "/tmp/Jammm_data/client.lzma".to_owned(); // TODO(#80)
        } else if arg.starts_with('{') {
            // This is for filling in the variables from the DataJson
            res = (&data[&arg[1..arg.len() - 1]].client).to_owned();
        } else {
            // Any other Argument is passed on directly to the "java"-call
            res = arg.to_owned();
        }
        if RE.is_match(&res) {
            // check if there is a maven string in the argument and resolve if necessary
            println!("{}", &res);
            res = path
                .join(
                    MINECRAFT_LIBRARIES_FOLDER.to_owned()
                        + &install_utils::process_maven_string(&res),
                )
                .as_os_str()
                .to_str()
                .unwrap()
                .to_owned();
        }
        args.push(res);
    }

    args
}

async fn download_libraries(libraries: &[LibraryJson], path: &Path) {
    for lib in libraries.iter() {
        let library = install_utils::Lib {
            name: &lib.name,
            path: &Path::new(MINECRAFT_LIBRARIES_FOLDER).join(&lib.downloads.artifact.path),
            url: &lib.downloads.artifact.url,
        };
        install_utils::download_lib(&library, path).await.unwrap();
    }
}
