use std::path::Path;

use anyhow::Result;

use crate::install_utils::{
    self, download_lib, Lib, MainJson, VersionJson, MINECRAFT_LIBRARIES_FOLDER,
};

pub async fn install_forge_method1(forge_json: MainJson, path: &Path) -> Result<()> {
    let version_json: VersionJson =
        serde_json::from_str(forge_json.version_json.as_ref().unwrap())?;
    install_utils::configure_version(&forge_json, path).await;
    for lib in version_json.libraries {
        if let Some(download_url) = lib.url {
            let resolved_maven = &install_utils::process_maven_string(&lib.name);
            let library = Lib {
                name: &lib.name,
                path: &Path::new(MINECRAFT_LIBRARIES_FOLDER).join(resolved_maven),
                url: &(download_url + "libraries/" + resolved_maven),
            };
            download_lib(&library, path).await?;
        }
    }
    let forge = Lib {
        name: "forge",
        path: &Path::new(MINECRAFT_LIBRARIES_FOLDER).join(&install_utils::process_maven_string(
            &forge_json.maven_version_string.unwrap(),
        )),
        url: &forge_json.download_url.unwrap(),
    };
    download_lib(&forge, path).await?;

    Ok(())
}
