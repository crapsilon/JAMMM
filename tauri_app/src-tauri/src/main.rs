#![cfg_attr(
  all(not(debug_assertions), target_os = "windows"),
  windows_subsystem = "windows"
)]

use tauri::async_runtime;

use jammm_core::folder_checker::create_all_folders;

fn main() {
  async_runtime::spawn(pre_start_initialization());

  tauri::Builder::default()
    .invoke_handler(tauri::generate_handler![install_modpack])
    .run(tauri::generate_context!())
    .expect("error while running tauri application");
}

#[tauri::command]
fn install_modpack(addon_id: u32, file_id: Option<u32>) {
  println!("{:?}, {:?}", addon_id, file_id);
}

async fn pre_start_initialization() {
  if let Err(err) = create_all_folders().await {
    println!("{:?}", err); // TODO (#83)
  }
}
