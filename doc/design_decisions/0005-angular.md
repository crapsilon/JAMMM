# 5. angular

Date: 2021-08-06

## Status

Accepted

See [4. Tauri App](0004-tauri-app.md)

## Decision

We use [Angular](https://angular.io/) for implementing the GUI application.
In addition to this [Material Angular](https://material.angular.io/) will be used.

## Consequences

* only Angular specific frontend libraries may be used
* Typescript will be used for the implementation
