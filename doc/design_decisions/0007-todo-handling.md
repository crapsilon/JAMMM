# 7. Record architecture decisions

Date: 2021-11-19

## Status

Accepted

## Context

We need to have a way to fix TODOs and not forget about anything.

## Decision

Every TODO/FIXME/etc. must have a issue number in brackets.

For example:

```ts
const foo: string = "Hello, World!" // TODO (42)
```

## Consequences

The ci pipeline should check if every TODO has the number behind it
and additionally check if an issue with this number exists.
