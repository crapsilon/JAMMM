# 8. Styleguide in frontend

Date: 2021-09-28

## Status

Accepted

## Context

Use the Airbnb ESLint-Styleguide in the frontend.

## Decision

This Styleguide ensures more consistent code quality.

## Consequences

It makes the code more readable and maintainable due to the strict ESLint-Rules but it could be also annoying for the developer in some cases.
