# 2. Projet Structure

Date: 2021-08-06

## Status

Accepted

See [3. Monorepo](0003-monorepo.md)

## Decision

The project will be divided into three parts:

* Tauri app: GUI for creating, downloading, maintaining and starting modpacks
* CLI app: CLI for downloading and starting modpacks and creating and starting servers from modpacks
* core-library: Rust shared library with the implementation of all the above functions

## Consequences

* all three parts must be compiled using the exact same version of the rust compiler (same commit of rustc)
* functionality must be implemented only once
* CLI and GUI can be developed and installed independently
