# 3. Monorepo

Date: 2021-08-06

## Status

Accepted

See [2. Project Structure](0002-projet-structure.md)

## Decision

We use a single repo with all parts of the project in sub-folders (e.g. `tauri_app`, `cli_app`, `core_library`, `doc`).

## Consequences

* build will be easier
* all information is in the same place
* issues and PRs may become confusing
