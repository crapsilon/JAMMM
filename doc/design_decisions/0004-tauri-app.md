# 4. Tauri App

Date: 2021-08-06

## Status

Accepted

See [5. angular](0005-angular.md)

## Decision

We use [Tauri](https://tauri.studio) for the GUI application.

## Consequences

* rust will be used for the library and the cli app
* graphical interface can be developed with web technologies
* size of the application-bundle will be relatively small compared to Electron
