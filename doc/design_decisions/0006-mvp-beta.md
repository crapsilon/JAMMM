# 6. MVP Beta

Date: 2021-08-06

## Contact

[crapStone](https://codeberg.org/crapStone)

## Status

in Discussion

## Context

MVP for the first first beta release.

## Decision

### Communication Frontend/Backend

* install modpack
* install mod for modpack
* list local modpacks
* get info of modpack
* check for updates of mods in modpack
* check for updates of installed modpacks
* update mod
* update modpack
* update all mods
* start modpack
* create modpack
* import modpack
* export modpack

### Frontend Features

* list installed modpacks
  * create new modpack
    * MC version
    * modloader
    * modloader version
  * start modpack
  * add mods
    * only show compatible mods (Forge/Fabric)
    * search
    * filter
      * MC version
      * mod type
  * detail view
* explore Curse modpacks
  * search
  * filter
    * MC version
    * modpack type
  * detail view
* import/export modpack
  * ensure Curse/Twitch compatibility
  * export
    * select folders/files to export

### Library Features

> see frontend above
