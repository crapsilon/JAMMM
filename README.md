<img src="doc/assets/jar.svg" height=150>

# JAMMM

[![License: GPL3+](https://img.shields.io/badge/License-GPL3+-blue.svg?style=flat-square)](https://www.gnu.org/licenses/gpl-3.0.en.html)
[![Ci Status](https://ci.codeberg.org/api/badges/crapsilon/JAMMM/status.svg?branch=develop)](https://ci.codeberg.org/crapsilon/JAMMM/)

JAMMM stands for **J**ust **A**nother **M**inecraft **M**od **M**anager

***This is a very early state of the project. There is only a simple frontend which uses the CurseForge API to get the modpack and mod information. At this time there is no actual logic to download or manage modpacks. So please be patient.***

## Authors

- [@Epsilon_02](https://codeberg.org/Epsilon_02)
- [@crapStone](https://codeberg.org/crapStone)
- [@CodingFish](https://codeberg.org/CodingFish)

## Roadmap

- [x] Light/dark mode toggle
- [ ] Cross platform (Linux/Windows/Mac not tested yet)
- [ ] Create own Modpacks
- [ ] Download Modpacks from Curse
- [ ] Update Mods and Modpacks
- [ ] Start Modpack with official Minecraft Launcher (just like the CurseForge Launcher)
- [ ] Create Modpack Templates for faster Modpack Bootstrapping
- [ ] Create Modpack Server directly from Modpack (remove automatically Client only Mods)

## Run Locally

Clone the project

```bash
  git clone https://codeberg.org/crapsilon/JAMMM.git
```

Go to the project directory

```bash
  cd jammm/tauri_app
```

This project is based on Tauri.
Please follow [this guide](https://tauri.studio/en/docs/getting-started/intro#setting-up-your-environment) to set up your dev-Environment to use Tauri properly.

Install dependencies

```bash
  yarn install
```

Start the dev server

```bash
  yarn start
```

To start Tauri you need to run

```bash
  yarn tauri:dev
```

## Screenshots

Screenshots are planned

![App Screenshot](https://via.placeholder.com/468x300?text=App+Screenshot+Here)
